package com.charter.rewards.model;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class Order {
	
	private String itemName;
	private BigDecimal price;
	private LocalDateTime purchaseDate;
}

package com.charter.rewards.model;

import lombok.Data;

import java.util.Map;

@Data
public class RewardResponse {

    private Long totalRewards;
    private Map<String, Long> rewardsPerMonth;
}

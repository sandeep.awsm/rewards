package com.charter.rewards.service;

import com.charter.rewards.exception.OrderNotFoundException;
import com.charter.rewards.model.Order;
import com.charter.rewards.model.RewardResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.YearMonth;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RewardService {

	public static final int MIN_ELIGIBLE_AMOUNT_FOR_DISCOUNT = 50;
	public static final int MAX_ELIGIBLE_AMOUNT_FOR_DISCOUNT = 100;

	@Value("${rewards.compute.max.months}")
	private Integer maxMonths;

	public RewardResponse calculateRewardPoint(List<Order> orders) throws OrderNotFoundException {

		if(orders == null || orders.isEmpty()) {
			throw new OrderNotFoundException("Orders not found!");
		}

		RewardResponse rewardResponse = new RewardResponse();
		calculateByMonthlyRecords(orders, rewardResponse);
		return rewardResponse;
	}

	private void calculateByMonthlyRecords(List<Order> orders, RewardResponse rewardResponse) {
		Map<String, Long> rewardsPerMonth = new HashMap<>();
		Long totalReward = 0L;

		for(int i=0; i < maxMonths; i++) {
			YearMonth month = YearMonth.now().minusMonths( i );
			String monthName = month.getMonth().name();
			Long rewardForMonth = computeRewardPointByOrders(orders, month);
			totalReward = totalReward+ rewardForMonth;
			rewardsPerMonth.put(monthName, rewardForMonth);
		}

		rewardResponse.setTotalRewards(totalReward);
		rewardResponse.setRewardsPerMonth(rewardsPerMonth);
	}

	private Long computeRewardPointByOrders(List<Order> orders, YearMonth month) {
		return orders.stream()
				.filter(order ->
						order.getPurchaseDate().toLocalDate().isAfter(month.atDay(1))
								&& order.getPurchaseDate().toLocalDate().isBefore(month.atEndOfMonth()))
				.mapToLong(order -> {
					if(order.getPrice().longValue() > MAX_ELIGIBLE_AMOUNT_FOR_DISCOUNT) {
						return (((order.getPrice().longValue() - MIN_ELIGIBLE_AMOUNT_FOR_DISCOUNT) * 1) +
								((order.getPrice().longValue() - MAX_ELIGIBLE_AMOUNT_FOR_DISCOUNT)) * 1);
					} else if(order.getPrice().longValue() > MIN_ELIGIBLE_AMOUNT_FOR_DISCOUNT){
						return ((order.getPrice().longValue() - MIN_ELIGIBLE_AMOUNT_FOR_DISCOUNT) * 1);
					} else {
						return 0;
					}
				}).sum();
	}

}
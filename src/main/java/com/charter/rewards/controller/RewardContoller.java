package com.charter.rewards.controller;

import com.charter.rewards.exception.OrderNotFoundException;
import com.charter.rewards.model.Order;
import com.charter.rewards.model.RewardResponse;
import com.charter.rewards.service.RewardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(path="/api/v1/reward")
public class RewardContoller {

    @Autowired
    RewardService rewardService;

    /**
     * Service will fetch customer total rewards from specified previous duration and also monthly rewards
     *
     *
     * @return OK/200 - is returned if NO data returned
     * @return Not_Found/404 - is returned if invalid argument is passed
     * @return Internal_server/500 - is returned if server error
     *
     * @exception OrderNotFoundException - Global level exception handling
     *
     */
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "compute")
    public ResponseEntity<RewardResponse> calculateRewardPoint(@RequestBody List<Order> orders) throws OrderNotFoundException {
        RewardResponse result = rewardService.calculateRewardPoint(orders);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ExceptionHandler(OrderNotFoundException.class)
    public final ResponseEntity<Object> handleOrderNotFoundException(OrderNotFoundException e) {
        log.error("Exception at: ", e);
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler({Exception.class})
    public final ResponseEntity<Object> handleGenericException(Exception e) {
        log.error("Exception at: ", e);
        return new ResponseEntity<>("Server error!", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

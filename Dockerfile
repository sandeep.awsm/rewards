FROM openjdk:11
EXPOSE 8080

ADD target/rewards-0.1.jar rewards.jar
ENTRYPOINT exec java -jar rewards.jar
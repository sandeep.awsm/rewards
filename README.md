# Reward Calculator
Reward Calculator is the microservice to calculate reward points

## How To Run In Docker Container

- Locate a Docker File and run following command to build a tag 
  - ```docker build -t rewards-0.1:latest```

- Bind port 8080 and add build tag 
  - ```docker run -p 8080:8080 rewards-0.1.jar:latest```

## How To Run Locally As Spring Boot Application

## Prerequisites

Ensure that the java>=8
```
java --version
````

## How To Run Locally
- Through IntelliJ/STS/Eclipse import as maven, spring boot app.
- do gradlew clean build
  - ```./gradlew clean build```
- Then run application as spring boot app.

![img.png](doc/buildlocal.png)

## Request Collection

- URL
  - ```POST: http://localhost:8080/api/v1/reward/compute```
  
- Sample Request Body
  - ```[

    {
        "itemName" : "apple",
        "purchaseDate" : "2022-06-03T10:15:30",
        "price" : 120
    },

    {
        "itemName" : "apple",
        "purchaseDate" : "2022-06-02T10:15:30",
        "price" : 120
    },

    {
        "itemName" : "apple",
        "purchaseDate" : "2022-05-02T10:15:30",
        "price" : 130
    }
    ]
- Sample Response
  - ```{
    "totalRewards": 290,
    "rewardsPerMonth": {
        "JUNE": 180,
        "MAY": 110,
        "APRIL": 0
     }
    }
    

## Health
- ```GET: http://localhost:8080/actuator/health```

![img.png](doc/health.png)